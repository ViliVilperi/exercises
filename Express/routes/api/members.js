const express = require('express');
const router = express.Router();
const uuid = require('uuid');
const members = require('../../Members'); // Haetaan samasta kansioissa oleva tiedosto Members

router.get('/', (req, res) => res.json(members)); // Automaattisesti muuntaa javascriptin objectin json muotoon

// Haetaan yksittäinen membu
router.get('/:id', (req, res) => {
    const loydetty = members.some(member => member.id === parseInt(req.params.id));

    if(loydetty){
        res.json(members.filter(member => member.id === parseInt(req.params.id)));
    }else{
        res.status(400).json({msg: `Kyseistä membua ei löytynyt id:llä: ${req.params.id}.`})
    }
});

// Luodaan membu
router.post('/', (req, res) => {
    // res.send(req.body); //Tämä lähetti saadun jsonin takaisin 

    const newMember = {
        id: uuid.v4(),
        name: req.body.name,
        email: req.body.email,
        status: 'active'
    }

    if(!newMember.name || !newMember.email){
        return res.status(400).json({ msg: 'Laita se nimi ja maili siihe jsoniin!!'});
    }

    members.push(newMember);
    res.json(members);
    //res.redirect('/')
});

//Päivitä membun tietoja
router.put('/:id', (req, res) => {
    const loydetty = members.some(member => member.id === parseInt(req.params.id));

    if(loydetty){
        const updMember = req.body;
        members.forEach(member => {
            if(member.id === parseInt(req.params.id)){
                member.name = updMember.name ? updMember.name : member.name;
                member.email = updMember.email ? updMember.email : member.email;
                
                res.json({msg: 'Member was updated', member});
            }

        });
    }else{
        res.status(400).json({msg: `Kyseistä membua ei löytynyt id:llä: ${req.params.id}.`})
    }
});

// Poista membu
router.delete('/:id', (req, res) => {
    const loydetty = members.some(member => member.id === parseInt(req.params.id));

    if(loydetty){
        res.json({msg: 'Membu poistettu', members:members.filter(member => member.id !== parseInt(req.params.id))});
    }else{
        res.status(400).json({msg: `Kyseistä membua ei löytynyt id:llä: ${req.params.id}.`})
    }
});

module.exports = router;