const ep = require('express'); //Haetaan expressi
const path = require('path');
const logger = require('./middleware/logger');
const exphbs = require('express-handlebars');
const members = require('./Members');


const app = ep();

//HandleBars middleware
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');


// Eli laitetaan middleware päälle
// app.use(logger);

// Body parser middleware
app.use(ep.json());
app.use(ep.urlencoded({extended: false}));

// Homepage route
app.get('/', (req, res) => res.render('index', {title: 'Member App', members}));


// app.get('/', (req, res) => {
//     // res.send('<h1>Hello World. No tere tere</h1>'); //Lähettää siis vastauksen takaisin selaimelle
//     res.sendFile(path.join(__dirname, 'public', 'index.html'));
// });

// Staattisen kansion luominen
app.use(ep.static(path.join(__dirname, 'public')));

// Tällä haetaan api käyttöön
app.use('/api/members', require('./routes/api/members'))

const PORT = process.env.PORT || 5000; // Luodaan portille oma osoite. Ensin tarkistetaan onko ypäristöllä omaa porttia jos ei ole käytetään omaa porttinumeroa

app.listen(PORT, () => console.log(`Serveri on aloitettu portilla ${PORT}`));