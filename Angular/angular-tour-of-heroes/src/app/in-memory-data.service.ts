import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Mr Kiva' },
      { id: 12, name: 'Narkkari' },
      { id: 13, name: 'Yrjöä' },
      { id: 14, name: 'Selleri' },
      { id: 15, name: 'Magneetti' },
      { id: 16, name: 'KumiMies' },
      { id: 17, name: 'Dynamo' },
      { id: 18, name: 'Small Brain' },
      { id: 19, name: 'Laava' },
      { id: 20, name: 'Tornaado' }
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}