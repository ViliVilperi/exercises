import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Mr Kiva' },
    { id: 12, name: 'Narkkari' },
    { id: 13, name: 'Yrjöä' },
    { id: 14, name: 'Selleri' },
    { id: 15, name: 'Magneetti' },
    { id: 16, name: 'KumiMies' },
    { id: 17, name: 'Dynamo' },
    { id: 18, name: 'Small Brain' },
    { id: 19, name: 'Laava' },
    { id: 20, name: 'Tornaado' }
]