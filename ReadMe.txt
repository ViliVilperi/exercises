Vili Raunola

This repository includes:
    1. Materials done in the exercise projects
    2. Learning diary
    3. My own project
    4. This ReadMe file
    5. Link to YouTube video where I show my project running
    
To run my project locally:
    1. Clone this repository to your own machine
    2. Go to "exercises/myproject"
    3. Install Node modules by running "npm i"
    4. Start the server by running the command "node app.js"
    5. After the server is built and running go to browser and search "http://localhost:8080"
    
    In this configuration the server will contact a Mongo database that is ran at Mongo's cloud service and is the same one that is used on the live version. 
    To change this you can go to "exercises/myproject/config/database.js" where you can define where the database connection will be made. 
    
Link to my introduction video: https://youtu.be/MIu8OPuEB0w

Link to the active build: https://grim-dungeon-81602.herokuapp.com/dashboard

Link to my learning diary: https://lut-my.sharepoint.com/:w:/g/personal/vili_raunola_student_lut_fi/ESY5s2RwwMVGvr_mAqpN6wkBwAet0FolyeDCf7mm40NL5A?e=A0NVJy